import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()
print("Bingo bango bongo")

while True:
    try:
        def process_approval(ch, method, properties, body):
            print("Received:", body)
            info = json.loads(body)
            send_mail(
                'Your presentation has been accepted',
                f"{info['presenter_name']}, we're happy to tell you that your presentation {info['title']} has been accepted",  # noqa E501
                'admin@conference.go',
                [info['presenter_email']],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            print("Received:", body)
            info = json.loads(body)
            send_mail(
                'Your presentation has been rejected',
                f"{info['presenter_name']}, we're sorry to tell you that your presentation {info['title']} has been rejected",  # noqa E501
                'admin@conference.go',
                [info['presenter_email']],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ. Trying again in 2 seconds")
        time.sleep(2.0)
