import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {
            "picture_url": content["photos"][0]["src"]["original"]
        }
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state_name):
    # print("City is:", city)
    # print("State is:", state_name)

    # Assign a the API endpoint url to a geocode_url variable
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state_name}&appid={OPEN_WEATHER_API_KEY}"  # noqa: E501
    # Make a get request and put the response in a geocode_response variable
    geocode_response = requests.get(geocode_url)
    # Use json.loads() to convert that response
    geocode_array = json.loads(geocode_response.content)
    # After looking at the shape of the data, extract the lat and long
    lat = geocode_array[0]["lat"]
    lon = geocode_array[0]["lon"]
    # Use the lat and long in the weather_url API endpoint
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"  # noqa: E501
    # Make a get requst and put the response in a weather_response variable
    weather_response = requests.get(weather_url)
    # Use json.loads() to convert that response to a variable
    weather_obj = json.loads(weather_response.content)
    # print(weather_obj["weather"][0]["description"])
    # print(weather_obj["main"]["temp"])
    try:
        return {
                "temp": weather_obj["main"]["temp"],
                "description": weather_obj["weather"][0]["description"],
            }
    except (KeyError, IndexError):
        return {
            "weather": None,
        }
